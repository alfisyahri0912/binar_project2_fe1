getSplitName = (personName) =>{
    // validasi data
    if(typeof personName !== "string"){
        return "Error : Name should be in string"
    }

    // 
    const splittedName = personName.split(" ")
    if(splittedName.length > 3){
        return "Error : Name should be 3 character or above"
    }else if(splittedName.length == 3){
        return {
            firstName : splittedName[0],
            middleName : splittedName[1],
            lastName : splittedName[2]
        }
    }else if(splittedName.length == 2){
        return {
            firstName : splittedName[0],
            middleName : null,
            lastName : splittedName[1]
        }
    }else if(splittedName.length == 1){
        return {
            firstName : splittedName[0],
            middleName : null,
            lastName : null
        }
    }
}

console.log(getSplitName("Adi Daniela Pranata"))
console.log(getSplitName("Dwi Kuncoro"))
console.log(getSplitName("Aurora"))
console.log(getSplitName("Aurora Aureliya Sukma Darma"))
console.log(getSplitName("Aurora Aureliya Sukma "))
console.log(getSplitName(0))