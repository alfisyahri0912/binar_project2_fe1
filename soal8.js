const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ]

// const toIDR = (value) => {
//   return new Intl.NumberFormat('id-ID', {
//     style: 'currency',
//     currency: 'IDR',
//   }).format(value)
// }

const getInfoPenjualan = (dataPenjualan) => {
  // validasi tipe data
  if(!Array.isArray(dataPenjualan)){
    return "Error : data penjualan harus bertipe array"
  }

  // menghitung total keuntungan
  const totalUntung = dataPenjualan.reduce((total, product) => {
    const untung =
      product.totalTerjual * (product.hargaJual - product.hargaBeli) -
      product.hargaBeli * product.sisaStok

    return total + untung
  }, 0)

  // menghitung total modal
  const totalModal = dataPenjualan.reduce((total, product) => {
    const modal = (product.totalTerjual + product.sisaStok) * product.hargaBeli

    return total + modal
  }, 0)

  //   menghitung persentase keuntungan
  const persenUntung = (totalUntung / totalModal) * 100

  // menentukan produk buku terlaris
  const productTerlaris = dataPenjualan.reduce((terlaris, currentProduct) => {
    return currentProduct.totalTerjual > terlaris.totalTerjual ? currentProduct: terlaris
  })

  //   menentukan penulis terlaris
  const penulis = []
  dataPenjualan.forEach((product) => {
    let isAvailable = false

    penulis.forEach((p) => {
      if (product.penulis === p.nama) {
        isAvailable = true
        p.terjual += product.totalTerjual
      }
    })

    if (!isAvailable) {
      penulis.push({
        nama: product.penulis,
        terjual: product.totalTerjual,
      })
    }
  })


  const penulisTerlaris = penulis.reduce((terlaris, currentPenulis) => {
    return currentPenulis.terjual > terlaris.terjual ? currentPenulis : terlaris
  })

  //   assign value pada masing-masing properti
  const infoPenjualan = {
    totalKeuntungan: "Rp. " + totalUntung.toLocaleString("id-ID"),
    totalModal: "Rp. " + totalModal.toLocaleString("id-ID"),
    persentaseKeuntungan: `${Math.ceil(persenUntung)}%`,
    produkBukuTerlaris: productTerlaris.namaProduk,
    penulisTerlaris: penulisTerlaris.nama
  }

  return infoPenjualan
}

console.log(getInfoPenjualan(dataPenjualanNovel))