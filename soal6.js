function getAngkaTerbesarKedua(angka){
    // validasi data
    if(angka === undefined){
        return "Error : Please input the array parameter"
    }
    if(!Array.isArray(angka)){
        return "Error : Angka should be array type"
    }
    if(angka.length < 2){
        return "Error : Array lenght should be 2 index or more"
    }

    
    return angka.sort()[angka.length-2]
}


const dataAngka = [9,4,7,7,4,3,2,2,8]

console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())