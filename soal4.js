function isValidPassword(password){
    // validasi tipe data
    if(password === undefined){
        console.log ("Error : Please input the password")
        return false 
    }else if(typeof password !== "string"){
        console.log ("Error : Password should be in string")
        return false
    }

    // proses
    const isValid = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/.test(password)
    return isValid
}

console.log(isValidPassword('Meong@2021'))
console.log(isValidPassword('meong2021'))
console.log(isValidPassword('@eong'))
console.log(isValidPassword('Meong2'))
console.log(isValidPassword(0))
console.log(isValidPassword())