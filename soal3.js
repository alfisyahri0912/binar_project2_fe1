function checkEmail(email){
    // validasi tipe data
    if(email === undefined){
        return "Error : Please input the email address"
    }if(typeof email !== "string"){
        return "Error : Email address should be in string"
    }if(!/@/.test(email)){
        return "Error : Email address should contain '@' character"
    }

    // proses
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
        return "VALID"
    }else{
        return "INVALID"
    }
}

console.log(checkEmail("apranata@Binar.co.id"))
console.log(checkEmail("apranata@Binar.com"))
console.log(checkEmail("apranata@Binar"))
console.log(checkEmail("apranata"))
console.log(checkEmail(3322))
console.log(checkEmail())