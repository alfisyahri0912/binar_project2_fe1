const dataPenjualanPakAldi = [
    {
      namaProduct: 'Sepatu Futsal Nike Vapor Academy 8',
      hargaSatuan: 760000,
      kategori: 'Sepatu Sport',
      totalTerjual: 90,
    },
    {
      namaProduct: 'Sepatu Warrior Tristan Black Brown High',
      hargaSatuan: 960000,
      kategori: 'Sepatu Sneaker',
      totalTerjual: 37,
    },
    {
      namaProduct: 'Sepatu Warrior Tristan Maroon High ',
      kategori: 'Sepatu Sneaker',
      hargaSatuan: 360000,
      totalTerjual: 90,
    },
    {
      namaProduct: 'Sepatu Warrior Rainbow Tosca Corduroy',
      hargaSatuan: 120000,
      kategori: 'Sepatu Sneaker',
      totalTerjual: 90,
    },
]

getTotalPenjualan = (dataPenjualan) => {
  // validasi tipe data
  if(!Array.isArray(dataPenjualan)){
      return "Error : data penjualan harus bertipe array"
  }


  // proses
  const totalPenjualan = dataPenjualan.reduce((total, data) => {
      return total + data.totalTerjual
  }, 0)

  if(isNaN(totalPenjualan)) {
      return "Error : Wrong tipe of input"
  }else {
      return totalPenjualan
  }
}

console.log(getTotalPenjualan(dataPenjualanPakAldi))